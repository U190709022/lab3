public class FindPrimes {
    public static void main (String[] args) {
        int primeNumber = Integer.parseInt(args[0]);
        if (primeNumber == 1 || primeNumber == 0){
            System.out.println("prime number is not found.");
        }
        function(primeNumber);
    }
    public static void function(int primeNumber) {
        int num1, num2;
        for (num1 = 1; num1 <= primeNumber; num1++) {
            if (num1 == 1 || num1 == 0) {
                continue;
            }
            boolean bool = true ;
            for (num2 = 2; num2 <= num1 / 2; ++num2) {
                if (num1 % num2 == 0) {
                    bool = false;
                    break;
                }
            }if(bool)
                System.out.print(num1 + " ");
        }
    }
}
